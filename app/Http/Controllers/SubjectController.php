<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Subject;

class SubjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subjects = Subject::join('teachers', 'subjects.teacher_id', '=', 'teachers.id')
        ->select(['subjects.*', 'teacher_name'])->paginate(3);
        return view('subject.subject', [
            'subjects' => $subjects
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('subject.add_subject');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $subject_name = $request->input('subject_name');
        $teacher_name = $request->input('teacher_name');
        switch ($teacher_name) {
            case "Ngo Vu Duc":
                $teacher_id = 3;
                break;
            case "Ta Thi Kim Hue":
                $teacher_id = 1;
                break;
            case "Pham Van Tien":
                $teacher_id = 2;
                break;
            case "Vu Van Yem":
                $teacher_id = 4;
                break;
            case "Cung Thanh Long":
                $teacher_id = 5;
                break;
            case "Nguyen Viet Anh":
                $teacher_id = 6;
                break;
            default :
                $teacher_id = null;
        }
        Subject::insert([
            'teacher_id' => $teacher_id,
            'subject_name' => $subject_name
        ]);
        return redirect()->route('subjects.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $subject = Subject::join('teachers', 'subjects.teacher_id', '=', 'teachers.id')
        ->get(['subjects.*', 'teacher_name'])->find($id);
        return view('subject.edit_subject', [
            'subject' => $subject
        ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $subject = Subject::find($id);
        $value = $request->input('subject_name');
        $subject->subject_name = $value;
        $subject->save();
        return redirect()->route('subjects.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $subject = Subject::where('id', $id)->delete();
        return redirect()->route('subjects.index');
    }
}
