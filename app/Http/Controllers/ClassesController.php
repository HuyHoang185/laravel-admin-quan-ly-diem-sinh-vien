<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Classes;
use Illuminate\Support\Facades\DB;

class ClassesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function insert_class(){
        $data = [
            // ['term_id'=>'1','teacher_id'=>'4','subject_id'=>'1','class_id'=>'101111'],
            // ['term_id'=>'1','teacher_id'=>'1','subject_id'=>'2','class_id'=>'101112'],
            // ['term_id'=>'1','teacher_id'=>'2','subject_id'=>'3','class_id'=>'101113'],
            // ['term_id'=>'1','teacher_id'=>'3','subject_id'=>'4','class_id'=>'101114'],
            // ['term_id'=>'1','teacher_id'=>'4','subject_id'=>'5','class_id'=>'101115'],
            // ['term_id'=>'1','teacher_id'=>'5','subject_id'=>'6','class_id'=>'101116'],
            // ['term_id'=>'2','teacher_id'=>'6','subject_id'=>'1','class_id'=>'101117'],
            // ['term_id'=>'2','teacher_id'=>'6','subject_id'=>'2','class_id'=>'101118'],
            // ['term_id'=>'3','teacher_id'=>'4','subject_id'=>'1','class_id'=>'101111'],
        ];
        Classes::insert($data);
    }
    public function index()
    {
        $classes = Classes::join('terms', 'classes.term_id', '=', 'terms.id')
        ->join('teachers', 'classes.teacher_id', '=', 'teachers.id')
        ->join('subjects', 'classes.subject_id', '=', 'subjects.id')
        ->orderBy('id', 'asc')
        ->select(['classes.*', 'terms.term_name', 'teachers.teacher_name', 'subjects.subject_name'])
        ->paginate(5);
        return view('class.class', ['classes' => $classes]);
    }

    public function getSearchAjax(Request $request)
    {
        $query = $request->get('query');
        if ($query != null) {
            $classes = Classes::join('terms', 'classes.term_id', '=', 'terms.id')
            ->join('teachers', 'classes.teacher_id', '=', 'teachers.id')
            ->join('subjects', 'classes.subject_id', '=', 'subjects.id')
            ->orWhere('class_id', 'LIKE', "%{$query}%")
            ->orWhere('term_name', 'LIKE', "%{$query}%")
            ->orWhere('teacher_name', 'LIKE', "%{$query}%")
            ->orWhere('subject_name', 'LIKE', "%{$query}%")
            ->get(['classes.*', 'terms.term_name', 'teachers.teacher_name', 'subjects.subject_name']);
            return view('class.search_class', ['classes' => $classes]);
        } elseif ($query == null) {
            $classes = Classes::join('terms', 'classes.term_id', '=', 'terms.id')
            ->join('teachers', 'classes.teacher_id', '=', 'teachers.id')
            ->join('subjects', 'classes.subject_id', '=', 'subjects.id')
            ->orderBy('id', 'asc')
            ->select(['classes.*', 'terms.term_name', 'teachers.teacher_name', 'subjects.subject_name'])
            ->paginate(5);
            return view('class.search_class', ['classes' => $classes]);
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('class.add_class');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    //  return redirect()->route('classes.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $class = Classes::join('terms', 'classes.term_id', '=', 'terms.id')
        ->join('teachers', 'classes.teacher_id', '=', 'teachers.id')
        ->join('subjects', 'classes.subject_id', '=', 'subjects.id')
        ->orderBy('id', 'asc')
        ->get(['classes.*', 'terms.term_name', 'teachers.teacher_name', 'subjects.subject_name'])
        ->find($id);
        return view('class.edit_class', [
            'class' => $class
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $class = Classes::where('id', $id)->delete();
        return redirect()->route('classes.index');
    }
}
