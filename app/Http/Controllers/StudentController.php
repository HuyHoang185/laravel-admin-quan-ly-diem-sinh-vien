<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Student;
class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function insert_student()
    {
        $data = [
            // ['student_id'=>'20176645','student_name'=>'Nguyen Van Huy'],
            // ['student_id'=>'20176646','student_name'=>'Nguyen Dinh An'],
            // ['student_id'=>'20176647','student_name'=>'Le Huy An '],
            // ['student_id'=>'20176648','student_name'=>'Le Van Sy'],
            // ['student_id'=>'20186645','student_name'=>'Dang Tuan Long'],
            // ['student_id'=>'20186655','student_name'=>'Nguyen Minh Phuong'],
            // ['student_id'=>'20196685','student_name'=>'Vu Tung Lam'],
            // ['student_id'=>'20196649','student_name'=>'Ngo Hoang Duong'],
        ];
        Student::insert($data);
        echo "thanh cong";
    }
    public function index()
    {
        $students = Student::paginate(3);
        return view('student.student',[
            'students' => $students
        ]);
    }

    function getSearchAjax(Request $request)
    {
        if($request->get('query'))
        {
            $query = $request->get('query');
            $data = Student::where('student_name', 'LIKE', "%{$query}%")
            ->get();
            $output = '<ul class="dropdown-menu" style="display:block; position:relative">';
            foreach($data as $row)
            {
               $output .= ' <li><a href="data/'. $row->id .'">'.$row->student_name.'</a></li> ';
            }
           $output .= '</ul>';
           echo $output;
       }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function create()
    {
        return view('student.add_student');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $student_name = $request->input('student_name');
        Student::insert([
          'student_name' => $student_name
        ]);
        header('location:/admin/students');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function edit($id)
    {
        $student = Student::find($id);
        return view('student.edit_student',[
            'student' => $student
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $student = Student::find($id);
        $value = $request->input('student_name');
        $student->student_name = $value;
        $student->save();
        return redirect()->route('students.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $student = Student::where('id', $id)->delete();
        return redirect()->route('students.index');
    }
}
