<?php

namespace App\Http\Controllers;

use App\Http\Controllers\GradeController as ControllersGradeController;
use Illuminate\Http\Request;
use App\Models\Grade;

class GradeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function insert_grade(){
        $data = [
            // ['class_id'=>'1','student_id'=>'1','mid_grade'=>'5','final_grade'=>'7'],
            // ['class_id'=>'1','student_id'=>'2','mid_grade'=>'6','final_grade'=>'8'],
            // ['class_id'=>'1','student_id'=>'3','mid_grade'=>'4','final_grade'=>'6'],
            // ['class_id'=>'1','student_id'=>'4','mid_grade'=>'7','final_grade'=>'6'],
            // ['class_id'=>'2','student_id'=>'1','mid_grade'=>'6','final_grade'=>'7'],
            // ['class_id'=>'2','student_id'=>'2','mid_grade'=>'9','final_grade'=>'7'],
            // ['class_id'=>'2','student_id'=>'3','mid_grade'=>'7','final_grade'=>'6'],
            // ['class_id'=>'2','student_id'=>'4','mid_grade'=>'3','final_grade'=>'6'],
            // ['class_id'=>'3','student_id'=>'2','mid_grade'=>'6','final_grade'=>'3'],
            // ['class_id'=>'3','student_id'=>'3','mid_grade'=>'6','final_grade'=>'6'],
            // ['class_id'=>'3','student_id'=>'4','mid_grade'=>'7','final_grade'=>'3'],
            // ['class_id'=>'3','student_id'=>'5','mid_grade'=>'6','final_grade'=>'7'],
            // ['class_id'=>'4','student_id'=>'3','mid_grade'=>'10','final_grade'=>'6'],
            // ['class_id'=>'4','student_id'=>'4','mid_grade'=>'7','final_grade'=>'1'],
            // ['class_id'=>'4','student_id'=>'5','mid_grade'=>'10','final_grade'=>'6'],
            // ['class_id'=>'4','student_id'=>'6','mid_grade'=>'6','final_grade'=>'8'],
            // ['class_id'=>'5','student_id'=>'1','mid_grade'=>'8','final_grade'=>'9'],
            // ['class_id'=>'5','student_id'=>'2','mid_grade'=>'8','final_grade'=>'5'],
            // ['class_id'=>'5','student_id'=>'3','mid_grade'=>'6','final_grade'=>'2'],
            // ['class_id'=>'5','student_id'=>'4','mid_grade'=>'5','final_grade'=>'6'],
            // ['class_id'=>'5','student_id'=>'5','mid_grade'=>'7','final_grade'=>'7'],
            // ['class_id'=>'5','student_id'=>'6','mid_grade'=>'6','final_grade'=>'6'],
            // ['class_id'=>'6','student_id'=>'1','mid_grade'=>'6','final_grade'=>'4'],
            // ['class_id'=>'6','student_id'=>'2','mid_grade'=>'8','final_grade'=>'6'],
            // ['class_id'=>'6','student_id'=>'3','mid_grade'=>'6','final_grade'=>'2'],
            // ['class_id'=>'6','student_id'=>'4','mid_grade'=>'9','final_grade'=>'7'],
            // ['class_id'=>'6','student_id'=>'5','mid_grade'=>'6','final_grade'=>'3'],
            // ['class_id'=>'6','student_id'=>'6','mid_grade'=>'1','final_grade'=>'6'],
            // ['class_id'=>'6','student_id'=>'7','mid_grade'=>'7','final_grade'=>'7'],
            // ['class_id'=>'6','student_id'=>'8','mid_grade'=>'6','final_grade'=>'3'],
            // ['class_id'=>'7','student_id'=>'1','mid_grade'=>'7','final_grade'=>'6'],
            // ['class_id'=>'7','student_id'=>'2','mid_grade'=>'6','final_grade'=>'4'],
            // ['class_id'=>'7','student_id'=>'3','mid_grade'=>'9','final_grade'=>'7'],
            // ['class_id'=>'7','student_id'=>'4','mid_grade'=>'2','final_grade'=>'6'],
            // ['class_id'=>'8','student_id'=>'2','mid_grade'=>'7','final_grade'=>'7'],
            // ['class_id'=>'8','student_id'=>'4','mid_grade'=>'2','final_grade'=>'9'],
            // ['class_id'=>'8','student_id'=>'6','mid_grade'=>'7','final_grade'=>'9'],
            // ['class_id'=>'8','student_id'=>'8','mid_grade'=>'8','final_grade'=>'6'],
            // ['class_id'=>'9','student_id'=>'5','mid_grade'=>'6','final_grade'=>'7'],
            // ['class_id'=>'9','student_id'=>'6','mid_grade'=>'7','final_grade'=>'8'],
            // ['class_id'=>'9','student_id'=>'7','mid_grade'=>'6','final_grade'=>'6'],
            // ['class_id'=>'9','student_id'=>'8','mid_grade'=>'6','final_grade'=>'6'],
        ];
        Grade::insert($data);
        echo "thanh cong";
    }
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('grade.add_grade');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $student_name = $request->input('student_name');
        $subject_name = $request->input('subject_name');
        $teacher_name = $request->input('teacher_name');
        $term = $request->input('term');
        $grade = $request->input('grade');
        switch ($subject_name) {
            case "Thong Tin So 1":
                $subject_id = 2;
                break;
            case "Kien Truc May Tinh":
                $subject_id = 1;
                break;
            case "Xu Ly So Tin Hieu":
                $subject_id = 4;
                break;
            case "Thiet Ke So VHDL":
                $subject_id = 6;
                break;
            case "Mach Tuyen Tinh 2":
                $subject_id = 2;
                break;
            case "Co So Mang ":
                $subject_id = 5;
                break;
            default :
                $subject_id = null;
        }
        switch ($teacher_name) {
            case "Ngo Vu Duc":
                $teacher_id = 3;
                break;
            case "Ta Thi Kim Hue":
                $teacher_id = 1;
                break;
            case "Pham Van Tien":
                $teacher_id = 2;
                break;
            case "Vu Van Yem":
                $teacher_id = 4;
                break;
            case "Cung Thanh Long":
                $teacher_id = 5;
                break;
            case "Nguyen Viet Anh":
                $teacher_id = 6;
                break;
            default :
                $teacher_id = null;
        }
        switch ($term) {
            case "Ki 1":
                $term_id = 1;
                break;
            case "Ki 2":
                $term_id = 2;
                break;
            case "Ki 3":
                $term_id = 3;
                break;
            default :
                $term_id = null;
        }
        Grade::insert([
            'student_name' => $student_name,
            'subject_id' => $subject_id,
            'teacher_id' => $teacher_id,
            'term_id' => $term_id,
            'grade_value' => $grade
        ]);
        return redirect()->route('grades.index');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        $searchGrades = Grade::query()
            ->where('grades.class_id', $id)
            ->leftJoin('students', 'grades.student_id', '=', 'students.id')
            ->leftJoin('classes', 'grades.class_id', '=', 'classes.id')
            ->leftJoin('terms', 'classes.term_id', '=', 'terms.id')
            ->select(['grades.*', 'students.student_name', 'students.student_id', 'classes.class_id', 'terms.term_name']);
        if ($request->filled('student_id')) {
            $searchGrades->where('students.student_id', 'like', '%'. $request->input('student_id') .'%');
        }
        if ($request->filled('student_name')) {
            $searchGrades->where('students.student_name', 'like', '%'. $request->input('student_name') .'%');
        }
        $grades = $searchGrades->orderByDesc('grades.id')->paginate(20);
        return view('grade.grade', [
            'grades' => $grades,
            'id' => $id
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function edit($id)
    {
        $grade = Grade::query()
            ->where('grades.id', $id)
            ->leftJoin('students', 'grades.student_id', '=', 'students.id')
            ->leftJoin('classes', 'grades.class_id', '=', 'classes.id')
            ->leftJoin('terms', 'classes.term_id', '=', 'terms.id')
            ->select(['grades.*', 'students.student_name', 'teachers.teacher_name', 'terms.term_name']);
        return view('grade.edit_grade', [
            'grade' => $grade
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $student_name = $request->input('student_name');
        $subject_name = $request->input('subject_name');
        $teacher_name = $request->input('teacher_name');
        $term = $request->input('term');
        $grade_value = $request->input('grade');

        switch ($subject_name) {
            case "Thong Tin So 1":
                $subject_id = 2;
                break;
            case "Kien Truc May Tinh":
                $subject_id = 1;
                break;
            case "Xu Ly So Tin Hieu":
                $subject_id = 4;
                break;
            case "Thiet Ke So VHDL":
                $subject_id = 6;
                break;
            case "Mach Tuyen Tinh 2":
                $subject_id = 2;
                break;
            case "Co So Mang ":
                $subject_id = 5;
                break;
            default:
                $subject_id = null;
        }
        switch ($teacher_name) {
            case "Ngo Vu Duc":
                $teacher_id = 3;
                break;
            case "Ta Thi Kim Hue":
                $teacher_id = 1;
                break;
            case "Pham Van Tien":
                $teacher_id = 2;
                break;
            case "Vu Van Yem":
                $teacher_id = 4;
                break;
            case "Cung Thanh Long":
                $teacher_id = 5;
                break;
            case "Nguyen Viet Anh":
                $teacher_id = 6;
                break;
            default :
                $teacher_id = null;
        }
        switch ($term) {
            case "Ki 1":
                $term_id = 1;
                break;
            case "Ki 2":
                $term_id = 2;
                break;
            case "Ki 3":
                $term_id = 3;
                break;
            default :
                $term_id = null;
        }
        $grade = grade::find($id);
        $grade->student_name = $student_name;
        $grade->subject_id = $subject_id;
        $grade->teacher_id = $teacher_id;
        $grade->term_id = $term_id;
        $grade->grade_value = $grade_value;
        $grade->save();
        return redirect()->route('grades.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $grade = Grade::where('id', $id)->delete();
        return redirect()->route('grades.index');
    }
}
