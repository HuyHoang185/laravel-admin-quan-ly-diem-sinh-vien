<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\grade;

class grade_controller extends Controller
{
    public function grade(){
        return view('grade.grade');
    }
    public function get(){
        return grade::join('teachers','grades.teacher_id','=','teachers.id')
                     ->join('subjects','grades.subject_id','=','subjects.id')
                     ->join('terms','grades.term_id','=','terms.id')
                     ->get(['grades.*','teachers.teacher_name','subjects.subject_name','terms.term_name']);
    } 
    public function add_grade(){
        return view('grade.add_grade');
    }
    public function store(Request $request){
       $student_name=$request->input('student_name');
       $subject_name=$request->input('subject_name');
       $teacher_name=$request->input('teacher_name');
       $term=$request->input('term');
       $grade=$request->input('grade');
  
    switch($subject_name){
        case "Thong Tin So 1":$subject_id=2;break;
        case "Kien Truc May Tinh":$subject_id=1;break;
        case "Xu Ly So Tin Hieu":$subject_id=4;break;
        case "Thiet Ke So VHDL":$subject_id=6;break;
        case "Mach Tuyen Tinh 2":$subject_id=2;break;
        case "Co So Mang ":$subject_id=5;break;
        default :$subject_id=null;
    }
    switch($teacher_name){
        case "Ngo Vu Duc":$teacher_id=3;break;
        case "Ta Thi Kim Hue":$teacher_id=1;break;
        case "Pham Van Tien":$teacher_id=2;break;
        case "Vu Van Yem":$teacher_id=4;break;
        case "Cung Thanh Long":$teacher_id=5;break;
        case "Nguyen Viet Anh":$teacher_id=6;break;
        default :$teacher_id=null;
    }
    switch($term){
        case "Ki 1":$term_id=1;break;
        case "Ki 2":$term_id=2;break;
        case "Ki 3":$term_id=3;break;
        default :$term_id=null;
    }
    grade::insert([
        'student_name'=>$student_name,
        'subject_id'=>$subject_id,
        'teacher_id'=>$teacher_id,      
        'term_id'=>$term_id,
        'grade_value'=>$grade
    ]);
    header('location:grade');
    }
    public static $id =null;
    
    public static function edit_grade(Request $request){
        echo  grade_controller::$id=$request->id;      
        return view('grade.edit_grade');
    }
    public static function update(Request $request){
        $student_name=$request->input('student_name');
        $subject_name=$request->input('subject_name');
        $teacher_name=$request->input('teacher_name');
        $term=$request->input('term');
        $grade_value=$request->input('grade');
   
      switch($subject_name){
        case "Thong Tin So 1":$subject_id=2;break;
        case "Kien Truc May Tinh":$subject_id=1;break;
        case "Xu Ly So Tin Hieu":$subject_id=4;break;
        case "Thiet Ke So VHDL":$subject_id=6;break;
        case "Mach Tuyen Tinh 2":$subject_id=2;break;
        case "Co So Mang ":$subject_id=5;break;
        default :$subject_id=null;
    }
    switch($teacher_name){
        case "Ngo Vu Duc":$teacher_id=3;break;
        case "Ta Thi Kim Hue":$teacher_id=1;break;
        case "Pham Van Tien":$teacher_id=2;break;
        case "Vu Van Yem":$teacher_id=4;break;
        case "Cung Thanh Long":$teacher_id=5;break;
        case "Nguyen Viet Anh":$teacher_id=6;break;
        default :$teacher_id=null;
    }
    switch($term){
        case "Ki 1":$term_id=1;break;
        case "Ki 2":$term_id=2;break;
        case "Ki 3":$term_id=3;break;
        default :$term_id=null;
    }
    //   $id=$request->id;
    // echo  grade_controller::$id.'hello';
    $di=grade_controller::$id;
    echo $di;
      $grade=grade::find($di);
      $grade->student_name=$student_name;
      $grade->subject_id=$subject_id;
      $grade->teacher_id=$teacher_id;
      $grade->term_id=$term_id;
      $grade->grade_value=$grade_value;
      $grade->save();
    }
    public function create_data(){
        grade::create([
            // 'student_name'=>'Nguyen Van A',
            // 'subject_id'=>'1',
            // 'teacher_id'=>'1',
            // 'term_id'=>'1',
            // 'grade_value'=>'8'
        ]);
        // grade::create([
        //     'student_name'=>'Nguyen Van A',
        //     'subject_id'=>'2',
        //     'teacher_id'=>'2',
        //     'term_id'=>'1',
        //     'grade_value'=>'7'
        // ]);
        // grade::create([
        //     'student_name'=>'Nguyen Van A',
        //     'subject_id'=>'3',
        //     'teacher_id'=>'3',
        //     'term_id'=>'2',
        //     'grade_value'=>'9'
        // ]);
        // grade::create([
        //     'student_name'=>'Nguyen Van A',
        //     'subject_id'=>'4',
        //     'teacher_id'=>'4',
        //     'term_id'=>'2',
        //     'grade_value'=>'5'
        // ]);
        // grade::create([
        //     'student_name'=>'Nguyen Van A',
        //     'subject_id'=>'5',
        //     'teacher_id'=>'5',
        //     'term_id'=>'3',
        //     'grade_value'=>'4'
        // ]);
        // grade::create([
        //     'student_name'=>'Nguyen Van A',
        //     'subject_id'=>'6',
        //     'teacher_id'=>'6',
        //     'term_id'=>'3',
        //     'grade_value'=>'7'
        // ]);
        
        //     grade::create([
        //         'student_name'=>'Nguyen Van B',
        //         'subject_id'=>'1',
        //         'teacher_id'=>'1',
        //         'term_id'=>'1',
        //         'grade_value'=>rand(3,10)
        //     ]);
        //     grade::create([
        //         'student_name'=>'Nguyen Van B',
        //         'subject_id'=>'2',
        //         'teacher_id'=>'2',
        //         'term_id'=>'1',
        //         'grade_value'=>rand(3,10)
        //     ]);
        //     grade::create([
        //         'student_name'=>'Nguyen Van B',
        //         'subject_id'=>'3',
        //         'teacher_id'=>'3',
        //         'term_id'=>'2',
        //         'grade_value'=>rand(3,10)
        //     ]);
        //     grade::create([
        //         'student_name'=>'Nguyen Van B',
        //         'subject_id'=>'4',
        //         'teacher_id'=>'4',
        //         'term_id'=>'2',
        //         'grade_value'=>rand(3,10)
        //     ]);
        //     grade::create([
        //         'student_name'=>'Nguyen Van B',
        //         'subject_id'=>'5',
        //         'teacher_id'=>'5',
        //         'term_id'=>'3',
        //         'grade_value'=>rand(3,10)
        //     ]);
        //     grade::create([
        //         'student_name'=>'Nguyen Van B',
        //         'subject_id'=>'6',
        //         'teacher_id'=>'6',
        //         'term_id'=>'3',
        //         'grade_value'=>rand(3,10)
        //     ]);

        //     grade::create([
        //         'student_name'=>'Nguyen Van C',
        //         'subject_id'=>'1',
        //         'teacher_id'=>'1',
        //         'term_id'=>'1',
        //         'grade_value'=>rand(3,10)
        //     ]);
        //     grade::create([
        //         'student_name'=>'Nguyen Van C',
        //         'subject_id'=>'2',
        //         'teacher_id'=>'2',
        //         'term_id'=>'1',
        //         'grade_value'=>rand(3,10)
        //     ]);
        //     grade::create([
        //         'student_name'=>'Nguyen Van C',
        //         'subject_id'=>'3',
        //         'teacher_id'=>'3',
        //         'term_id'=>'2',
        //         'grade_value'=>rand(3,10)
        //     ]);
        //     grade::create([
        //         'student_name'=>'Nguyen Van C',
        //         'subject_id'=>'4',
        //         'teacher_id'=>'4',
        //         'term_id'=>'2',
        //         'grade_value'=>rand(3,10)
        //     ]);
        //     grade::create([
        //         'student_name'=>'Nguyen Van C',
        //         'subject_id'=>'5',
        //         'teacher_id'=>'5',
        //         'term_id'=>'3',
        //         'grade_value'=>rand(3,10)
        //     ]);
        //     grade::create([
        //         'student_name'=>'Nguyen Van C',
        //         'subject_id'=>'6',
        //         'teacher_id'=>'6',
        //         'term_id'=>'3',
        //         'grade_value'=>rand(3,10)
        //     ]);

        //     grade::create([
        //         'student_name'=>'Nguyen Van D',
        //         'subject_id'=>'1',
        //         'teacher_id'=>'1',
        //         'term_id'=>'1',
        //         'grade_value'=>rand(3,10)
        //     ]);
        //     grade::create([
        //         'student_name'=>'Nguyen Van D',
        //         'subject_id'=>'2',
        //         'teacher_id'=>'2',
        //         'term_id'=>'1',
        //         'grade_value'=>rand(3,10)
        //     ]);
        //     grade::create([
        //         'student_name'=>'Nguyen Van D',
        //         'subject_id'=>'3',
        //         'teacher_id'=>'3',
        //         'term_id'=>'2',
        //         'grade_value'=>rand(3,10)
        //     ]);
        //     grade::create([
        //         'student_name'=>'Nguyen Van D',
        //         'subject_id'=>'4',
        //         'teacher_id'=>'4',
        //         'term_id'=>'2',
        //         'grade_value'=>rand(3,10)
        //     ]);
        //     grade::create([
        //         'student_name'=>'Nguyen Van D',
        //         'subject_id'=>'5',
        //         'teacher_id'=>'5',
        //         'term_id'=>'3',
        //         'grade_value'=>rand(3,10)
        //     ]);
        //     grade::create([
        //         'student_name'=>'Nguyen Van D',
        //         'subject_id'=>'6',
        //         'teacher_id'=>'6',
        //         'term_id'=>'3',
        //         'grade_value'=>rand(3,10)
        //     ]);

        //     grade::create([
        //         'student_name'=>'Nguyen Van E',
        //         'subject_id'=>'1',
        //         'teacher_id'=>'1',
        //         'term_id'=>'1',
        //         'grade_value'=>rand(3,10)
        //     ]);
        //     grade::create([
        //         'student_name'=>'Nguyen Van E',
        //         'subject_id'=>'2',
        //         'teacher_id'=>'2',
        //         'term_id'=>'1',
        //         'grade_value'=>rand(3,10)
        //     ]);
        //     grade::create([
        //         'student_name'=>'Nguyen Van E',
        //         'subject_id'=>'3',
        //         'teacher_id'=>'3',
        //         'term_id'=>'2',
        //         'grade_value'=>rand(3,10)
        //     ]);
        //     grade::create([
        //         'student_name'=>'Nguyen Van E',
        //         'subject_id'=>'4',
        //         'teacher_id'=>'4',
        //         'term_id'=>'2',
        //         'grade_value'=>rand(3,10)
        //     ]);
        //     grade::create([
        //         'student_name'=>'Nguyen Van E',
        //         'subject_id'=>'5',
        //         'teacher_id'=>'5',
        //         'term_id'=>'3',
        //         'grade_value'=>rand(3,10)
        //     ]);
        //     grade::create([
        //         'student_name'=>'Nguyen Van E',
        //         'subject_id'=>'6',
        //         'teacher_id'=>'6',
        //         'term_id'=>'3',
        //         'grade_value'=>rand(3,10)
        //     ]);

        //     grade::create([
        //         'student_name'=>'Nguyen Van F',
        //         'subject_id'=>'1',
        //         'teacher_id'=>'1',
        //         'term_id'=>'1',
        //         'grade_value'=>rand(3,10)
        //     ]);
        //     grade::create([
        //         'student_name'=>'Nguyen Van F',
        //         'subject_id'=>'2',
        //         'teacher_id'=>'2',
        //         'term_id'=>'1',
        //         'grade_value'=>rand(3,10)
        //     ]);
        //     grade::create([
        //         'student_name'=>'Nguyen Van F',
        //         'subject_id'=>'3',
        //         'teacher_id'=>'3',
        //         'term_id'=>'2',
        //         'grade_value'=>rand(3,10)
        //     ]);
        //     grade::create([
        //         'student_name'=>'Nguyen Van F',
        //         'subject_id'=>'4',
        //         'teacher_id'=>'4',
        //         'term_id'=>'2',
        //         'grade_value'=>rand(3,10)
        //     ]);
        //     grade::create([
        //         'student_name'=>'Nguyen Van F',
        //         'subject_id'=>'5',
        //         'teacher_id'=>'5',
        //         'term_id'=>'3',
        //         'grade_value'=>rand(3,10)
        //     ]);
        //     grade::create([
        //         'student_name'=>'Nguyen Van F',
        //         'subject_id'=>'6',
        //         'teacher_id'=>'6',
        //         'term_id'=>'3',
        //         'grade_value'=>rand(3,10)
        //     ]);
            echo "created";
        }    
    
    public function update_data(){
        $grade=grade::find(39);
        $grade->student_name='';
        $grade->subject_id='';
        $grade->teacher_id='';
        $grade->term_id='';
        $grade->grade_value='';
        $grade->save();
        echo "updated";
    }
    public function delete_data(){
        grade::destroy(null);
        echo "deleted";
    }

}
