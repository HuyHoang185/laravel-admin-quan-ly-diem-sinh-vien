<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\grade;

class GradeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('grade.grade');
        
    }
    public function get(){
        return grade::join('teachers','grades.teacher_id','=','teachers.id')
                     ->join('subjects','grades.subject_id','=','subjects.id')
                     ->join('terms','grades.term_id','=','terms.id')
                     ->get(['grades.*','teachers.teacher_name','subjects.subject_name','terms.term_name']);
    } 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('grade.add_grade');     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $student_name=$request->input('student_name');
       $subject_name=$request->input('subject_name');
       $teacher_name=$request->input('teacher_name');
       $term=$request->input('term');
       $grade=$request->input('grade');
  
    switch($subject_name){
        case "Thong Tin So 1":$subject_id=2;break;
        case "Kien Truc May Tinh":$subject_id=1;break;
        case "Xu Ly So Tin Hieu":$subject_id=4;break;
        case "Thiet Ke So VHDL":$subject_id=6;break;
        case "Mach Tuyen Tinh 2":$subject_id=2;break;
        case "Co So Mang ":$subject_id=5;break;
        default :$subject_id=null;
    }
    switch($teacher_name){
        case "Ngo Vu Duc":$teacher_id=3;break;
        case "Ta Thi Kim Hue":$teacher_id=1;break;
        case "Pham Van Tien":$teacher_id=2;break;
        case "Vu Van Yem":$teacher_id=4;break;
        case "Cung Thanh Long":$teacher_id=5;break;
        case "Nguyen Viet Anh":$teacher_id=6;break;
        default :$teacher_id=null;
    }
    switch($term){
        case "Ki 1":$term_id=1;break;
        case "Ki 2":$term_id=2;break;
        case "Ki 3":$term_id=3;break;
        default :$term_id=null;
    }
    grade::insert([
        'student_name'=>$student_name,
        'subject_id'=>$subject_id,
        'teacher_id'=>$teacher_id,      
        'term_id'=>$term_id,
        'grade_value'=>$grade
    ]);
    header('location:grade');
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('grade.edit_grade');
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // echo $id;

        return view('grade.edit_grade',['id'=> $id]);
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    { 
        echo $id;
    //     $student_name=$request->input('student_name');
    //     $subject_name=$request->input('subject_name');
    //     $teacher_name=$request->input('teacher_name');
    //     $term=$request->input('term');
    //     $grade_value=$request->input('grade');
   
    //   switch($subject_name){
    //     case "Thong Tin So 1":$subject_id=2;break;
    //     case "Kien Truc May Tinh":$subject_id=1;break;
    //     case "Xu Ly So Tin Hieu":$subject_id=4;break;
    //     case "Thiet Ke So VHDL":$subject_id=6;break;
    //     case "Mach Tuyen Tinh 2":$subject_id=2;break;
    //     case "Co So Mang ":$subject_id=5;break;
    //     default :$subject_id=null;
    // }
    // switch($teacher_name){
    //     case "Ngo Vu Duc":$teacher_id=3;break;
    //     case "Ta Thi Kim Hue":$teacher_id=1;break;
    //     case "Pham Van Tien":$teacher_id=2;break;
    //     case "Vu Van Yem":$teacher_id=4;break;
    //     case "Cung Thanh Long":$teacher_id=5;break;
    //     case "Nguyen Viet Anh":$teacher_id=6;break;
    //     default :$teacher_id=null;
    // }
    // switch($term){
    //     case "Ki 1":$term_id=1;break;
    //     case "Ki 2":$term_id=2;break;
    //     case "Ki 3":$term_id=3;break;
    //     default :$term_id=null;
    // }
    //   $grade=grade::find($id);
    //   $grade->student_name=$student_name;
    //   $grade->subject_id=$subject_id;
    //   $grade->teacher_id=$teacher_id;
    //   $grade->term_id=$term_id;
    //   $grade->grade_value=$grade_value;
    //   $grade->save();
    //   header('location:grade');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
