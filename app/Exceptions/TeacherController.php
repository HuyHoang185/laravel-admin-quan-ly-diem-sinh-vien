<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\teacher;

class TeacherController extends Controller
{   
    public function teacher(){
        return view('teacher.teacher');
    }
    public function get(){
        return teacher::all();
    }
    public function add_teacher(){
        return view('teacher.add_teacher');
    }
    public function store(Request $request){
        $teacher=$request->input('teacher_name');
        teacher::insert([
           'teacher_name'=>$teacher
        ]);
        header("location:teacher");
    }
    public function edit_teacher(){
        return view('teacher.add_teacher');
    }
    public function create_data(){
       teacher::create([
       // 'teacher_name' => 'Ta Thi Kim Hue'
       ]);
       // teacher::create([
       //     'teacher_name' => 'Pham Van Tien'
       // ]);
       // teacher::create([
       //     'teacher_name' => 'Ngo Vu Duc'
       // ]);
       // teacher::create([
       //     'teacher_name' => 'Vu Van Yem'
       // ]);
       // teacher::create([
       //     'teacher_name' => 'Cung Thanh Long'
       //     ]);
       // teacher::create([
       //     'teacher_name' => 'Nguyen Viet Anh'
       // ]);    
        echo "inserted";
    }
    public function update_data(){
        $teacher=teacher::find(null);
        $teacher->teacher_name='';
        $teacher->save();
        echo "updated";
    } 
    public function delete_data(){
        teacher::destroy(null);
    echo " deleted";    
    }
    public function relation(){
        $teacher=teacher::find(2)->teacher_id;
        print_r($teacher);
        // print_r($teacher);
    }
}
