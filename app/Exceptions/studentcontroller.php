<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Models\Student;

class StudentController extends Controller
{   
    public function student(){
        return view('student.student');
    }
    public function get(){
        return student::all();
    }
    public function add_student(){
       return view('student.add_student');
    }
    public function store(Request $request){
        $student_name=$request->input('student_name');
        student::insert([
          'student_name'=>$student_name
        ]);
        header('location:student');
    }
    public function edit_student(){
        return view('student.edit_student');
    }
    public function create_data(){
        //   student::create([
        // 'student_name' => 'Nguyen Van B'
        //   ]);
        // student::create([
        //     'student_name' => 'Nguyen Van c'
        // ]);
        // student::create([
        //     'student_name' => 'Nguyen Van D'
        // ]);
        // student::create([
        //     'student_name' => 'Nguyen Van E'
        // ]);
        // student::create([
        //     'student_name' => 'Nguyen Van F'
        //     ]);
        // student::create([
        //     'student_name' => 'Nguyen Van G'
        // ]);    
        echo "inserted";
    }
    public function update_data(){
        $student=student::find(null);
        $student->student_name='';
        $student->save();
        echo "updated";
    }
    public function delete_data(){
        student::destroy(null);
        echo " deleted";    
    }
}
