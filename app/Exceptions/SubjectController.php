<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\subject;

class SubjectController extends Controller
{   
    public function subject(){
        return view('subject.subject');
    }
    public function get(){
        return subject::join('teachers','subjects.teacher_id','=','teachers.id')
        ->get(['subjects.*','teachers.teacher_name']);
    }
    public function add_subject(){
        return view('subject.add_subject');
    }
    public function store(Request $request){
        $subject_name=$request->input('subject_name');
        $teacher_name=$request->input('teacher_name');
        switch($teacher_name){
            case "Ngo Vu Duc":$teacher_id=3;break;
            case "Ta Thi Kim Hue":$teacher_id=1;break;
            case "Pham Van Tien":$teacher_id=2;break;
            case "Vu Van Yem":$teacher_id=4;break;
            case "Cung Thanh Long":$teacher_id=5;break;
            case "Nguyen Viet Anh":$teacher_id=6;break;
            default :$teacher_id=null;
        }
        subject::insert([
            'teacher_id'=>$teacher_id,
           'subject_name'=>$subject_name
        ]);
        header('location:subject');
    }
    public function edit_subject(){
        return view('subject.eidt_subject');
    }
    public function create_data(){
        // subject::create([
        //     'teacher_id'=>'1',
        //     'subject_name'=>'Kien Truc May Tinh'
        // ]);
        // subject::create([
        //     'teacher_id'=>'2',
        //     'subject_name'=>'Thong Tin So 1'
        // ]);
        // subject::create([
        //     'teacher_id'=>'3',
        //     'subject_name'=>'Mach Tuyen Tinh 2'
        // ]);
        // subject::create([
        //     'teacher_id'=>'4',
        //     'subject_name'=>'Xu Ly So Tin Hieu'
        // ]);
        // subject::create([
        //     'teacher_id'=>'5',
        //     'subject_name'=>'Co So Mang'
        // ]);
        // subject::create([
        //     'teacher_id'=>'6',
        //     'subject_name'=>'Thiet Ke So VHDL'
        // ]);
        // echo "created";
    }
    public function update_data(){
        $subject=subject::find(null);
        $subject->teacher_id='';
        $subject->subject_name='';
        $subject->save();
        echo "updated";
    }
    public function delete_data(){
        subject::destroy(null);
        echo "deleled";
    }
  
}
