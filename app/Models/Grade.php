<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;


class Grade extends Model
{
    use HasFactory;
    protected $fillable = [
        'id_student',
        'student_id',
        'student_name',
        'subject_id',
        'teacher_id',
        'term_id',
        'grade_value'
    ];
    protected $dates = ['deleted_at'];
}
