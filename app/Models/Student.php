<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class student extends Model
{
    use HasFactory;
    protected $fillable = [
        'student_id',
        'student_name'
    ];
    protected $dates = ['deleted_at'];
}

