<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class subject extends Model
{
    use HasFactory;
    protected $fillable = [
        'teacher_id',
        'subject_name'
    ];
    protected $dates = ['deleted_at'];
}
