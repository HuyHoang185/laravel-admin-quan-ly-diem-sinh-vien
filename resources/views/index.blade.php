<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="{{ asset('css/admin.css') }}" rel="stylesheet" type="text/css" media="all">
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" media="all">
    <script src="{{ asset('js/jquery-3.5.1.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
</head>
<body>
    <!-- header -->
    <div id="header" >
       <div class="head">
          <div>
             <a href="{{ route('grades.index') }}"><span>Admin </span></a>
          </div>
       </div>
    </div>

    <!-- menu -->
    <div id="menu">
        <div class="container">
            <div class="row">
                <div class="col-lg-2 list ">
                    <a href="{{ route('classes.index') }}" class="">Class</a>
                </div>
                <div class="col-lg-2 list">
                    <a href="{{ route('students.index') }}" class="">Student</a>
                </div>
                <div class="col-lg-3 list">
                    <a href="{{ route('teachers.index') }}">Teacher</a>
                </div>
                <div class="col-lg-3 list">
                    <a href="{{ route('subjects.index') }}">Subject</a>
                </div>
                <div class="col-lg-2 list">
                    <a href="{{ route('terms.index') }}">Term</a>
                </div>
            </div>
        </div>
    </div>
  @yield('content')
</body>
</html>
