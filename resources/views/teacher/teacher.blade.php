@extends('index')
@section('content')
 <!-- body -->
 <div id="body">
     <div class="container">
         <a href="{{ route('teachers.create') }}" class="btn btn-success btn-sm active" role="button"
            aria-pressed="true">Thêm danh mục</a>
         <div class="row content">
             <table>
                 <tr>
                     <th>id</th>
                     <th>teacher_name</th>
                     <th>Action</th>
                 </tr>
                 @foreach($teachers as $teacher)
                     <tr>
                         <td>{{ $teacher->id }}</td>
                         <td>{{ $teacher->teacher_name }}</td>
                         <td style="position:relative">
                             <a href="{{ route('teachers.edit', ['teacher' => $teacher->id]) }}"
                                class="btn btn-primary btn-sm active" role="button" aria-pressed="true">Sửa</a>
                             <form action="{{ route('teachers.destroy', ['teacher' => $teacher->id]) }}" method="POST">
                                 {{ method_field('DELETE') }}
                                 {{ csrf_field() }}
                                 <button onclick="return del('{{ $teacher->teacher_name }}')"
                                         style="position:absolute;top:1px;right:70px"
                                         class="btn btn-danger btn-sm active">Xóa
                                 </button>
                             </form>
                         </td>
                     </tr>
                 @endforeach
             </table>
         </div>
     </div>
     <nav aria_label="Page navigation" style="margin:30px 0 0 75px">
         {{ $teachers->links() }}
     </nav>
 </div>
 <script>
     function del(name) {
         return confirm("bạn có chắc chắc muốn xóa bỏ không " + name + "?");
     }
 </script>
@endsection
