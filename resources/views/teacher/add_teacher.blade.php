@extends('index')
@section('content')
   <!-- body -->
   <div id="add_product">
        <div class="container">
            <div class=" row place">
                <p>Home | Teacher | Thêm thầy cô</p>
            </div>
            <div style="margin:40px 0 40px -15px;"><h3>Thêm thầy cô</h3></div>
            <div class="row">
                <div class="form-add">
                    <form method="post" enctype="multipart/form-data" action="{{ route('teachers.store') }}">
                    {{csrf_field()}}
                        <div class="form-group">
                            <label>Tên thầy cô</label>
                            <input name="teacher_name" type="text" value="">
                        </div>
                        <div class="form-group">
                            <input name="sbm" type="submit" value="Thêm mới">
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>

@endsection
