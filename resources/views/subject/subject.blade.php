@extends('index')
@section('content')
 <!-- body -->
 <div id="body">
     <div class="container">
         <a href="{{ route('subjects.create') }}" class="btn btn-success btn-sm active" role="button"
            aria-pressed="true">Thêm
             danh mục</a>
         <div class="row content">
             <table>
                 <tr>
                     <th>id</th>
                     <th>subject_name</th>
                     <th>teacher_name</th>
                     <th>Action</th>
                 </tr>
                 @foreach($subjects as $subject)
                     <tr>
                         <td>{{ $subject->id }}</td>
                         <td>{{ $subject->subject_name }}</td>
                         <td>{{ $subject->teacher_name }}</td>
                         <td style="position:relative">
                             <a href="{{ route('subjects.edit', ['subject' => $subject->id]) }}"
                                class="btn btn-primary btn-sm active"
                                role="button" aria-pressed="true">Sửa</a>
                             <form action="{{ route('subjects.destroy', ['subject' => $subject->id]) }}" method="POST">
                                 {{ method_field('DELETE') }}
                                 {{ csrf_field() }}
                                 <button onclick="return del('{{ $subject->subject_name }}')"
                                         style="position:absolute;top:1px;right:20px"
                                         class="btn btn-danger btn-sm active">Xóa
                                 </button>
                             </form>
                         </td>
                     </tr>
                 @endforeach
             </table>
         </div>
     </div>
     <nav aria_label="Page navigation" style="margin:30px 0 0 75px">
         {{ $subjects->links() }}
     </nav>
 </div>
<script>
  function del(name){
      return confirm("bạn có chắc chắc muốn xóa bỏ không "+name+"?");
  }
</script>
@endsection
