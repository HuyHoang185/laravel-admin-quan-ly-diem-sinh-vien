@extends('index')
@section('content')

  <!-- body -->
  <div id="edit_product">
      <div class="container">
          <div class=" row place">
              <p>Home | Subject | Sửa tên môn học</p>
          </div>
          <div style="margin:40px 0 40px -15px;"><h3>Sửa tên môn học</h3></div>
          <div class="row">
              <div class="form-add">
                  <form method="post" enctype="multipart/form-data"
                        action="{{ route('subjects.update', ['subject' => $subject->id]) }}">
                      @method('PUT')
                      {{csrf_field()}}
                      <div class="form-group">
                          <label>Tên môn học</label>
                          <input name="subject_name" type="text" value="{{ $subject->subject_name }}">
                      </div>
                      <div class="form-group">
                          <label>Tên giáo viên</label>
                          <input name="teacher_name" type="text" value="{{ $subject->teacher_name }}">
                      </div>
                      <div class="form-group">
                          <input name="sbm" type="submit" role="" value="Cập nhật">
                      </div>
                  </form>
              </div>
          </div>
      </div>
  </div>
@endsection
