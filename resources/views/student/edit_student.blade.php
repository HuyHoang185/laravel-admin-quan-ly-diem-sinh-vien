@extends('index')
@section('content')
    <!-- body -->
    <div id="edit_cat">
        <div class="container">
            <div class=" row place">
                <p>Home | Students | Sửa tên học sinh</p>
            </div>
            <div style="margin:40px 0 40px -15px;"><h3>Sửa tên học sinh</h3></div>
            <div class="row">
                <form class="form" method="post" action="{{ route('students.update', ['student' => $student->id]) }}">
                    @method('PUT')
                    {{csrf_field()}}
                    <label>Tên học sinh</label>
                    <input name="student_name" type="" value="{{ $student->student_name }}" placeholder="Tên học sinh">
                    <input name="sbm" type="submit" value="Cập nhật">
                </form>
            </div>
        </div>
    </div>
@endsection
