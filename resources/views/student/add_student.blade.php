@extends('index')
@section('content')
   <!-- body -->
   <div id="add_product">
       <div class="container">
           <div class=" row place">
               <p>Home | Student | Thêm học sinh</p>
           </div>
           <div style="margin:40px 0 40px -15px;"><h3>Thêm học sinh</h3></div>
           <div class="row">
               <div class="form-add">
                   <form method="post" enctype="multipart/form-data" action="{{ route('students.store') }}">
                       {{csrf_field()}}
                       <div class="form-group">
                           <label>Tên học sinh</label>
                           <input name="student_name" type="text" value="">
                       </div>
                       <div class="form-group">
                           <input name="sbm" type="submit" value="Thêm mới">
                       </div>
                   </form>
               </div>
           </div>
       </div>
   </div>
@endsection
