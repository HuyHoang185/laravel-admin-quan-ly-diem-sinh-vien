@extends('index')
@section('content')
 <!-- body -->
 <div id="body">
     <div class="container">
         <a href="{{ route('students.create') }}" class="btn btn-success btn-sm active" role="button"
            aria-pressed="true">Thêm danh mục</a>
         <div class="row content">
             <table>
                 <tr>
                     <th>id</th>
                     <th>mssv</th>
                     <th>student_name</th>
                     <th>Action</th>
                 </tr>
                 @foreach($students as $student)
                     <tr>
                         <td>{{ $student->id }} </td>
                         <td>{{ $student->student_id }} </td>
                         <td>{{ $student->student_name }}</td>
                         <td style="position:relative">
                             <a href=" {{ route('students.edit', ['student' => $student->id]) }}" class="btn btn-primary btn-sm active" role="button" aria-pressed="true">Sửa</a>
                             <form action="{{ route('students.destroy', ['student' => $student->id]) }}" method="POST">
                                 {{ method_field('DELETE') }}
                                 {{ csrf_field() }}
                                 <button onclick="return del('{{ $student->student_name }}')" style="position:absolute;top:1px;right:30px" class="btn btn-danger btn-sm active">Xóa</button>
                             </form>
                         </td>
                     </tr>
                 @endforeach
             </table>
         </div>
     </div>
     <nav aria_label="Page navigation" style="margin:30px 0 0 75px">
         {{ $students->links() }}
     </nav>
 </div>
<script>
  function del(name){
      return confirm("bạn có chắc chắc muốn xóa bỏ không "+name+"?");
  }
</script>
@endsection
