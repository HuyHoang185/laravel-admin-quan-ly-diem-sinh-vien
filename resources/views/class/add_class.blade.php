@extends('index')
@section('content')
   <!-- body -->
   <div id="add_product">
       <div class="container">
           <div class=" row place">
               <p>Home | Product | Thêm class</p>
           </div>
           <div style="margin:40px 0 40px -15px;"><h3>Thêm class</h3></div>
           <div class="row">
               <div class="form-add">
                   <form method="post" enctype="multipart/form-data" action="{{ route('classes.store') }}">
                       {{ csrf_field() }}
                       <div class="form-group">
                           <label>Tên kì học</label>
                           <input name="term_name" type="text" value="">
                       </div>
                       <div class="form-group">
                           <label>Tên giáo viên</label>
                           <input name="teacher_name" type="text" value="">
                       </div>
                       <div class="form-group">
                           <label>Tên môn học</label>
                           <input name="subject_name" type="text" value="">
                       </div>
                       <div class="form-group">
                           <label>lớp</label>
                           <input name="class_id" type="text" value="">
                       </div>
                       <div class="form-group">
                           <input name="sbm" type="submit" value="Thêm mới">
                       </div>
                   </form>
               </div>
           </div>
       </div>
   </div>

@endsection
