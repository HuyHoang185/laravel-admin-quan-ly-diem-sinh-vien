@foreach ($classes as $class)
    <tr>
        <td>{{ $class->id }}</td>
        <td>{{ $class->term_name }}</td>
        <td>{{ $class->teacher_name }}</td>
        <td>{{ $class->subject_name }}</td>
        <td style="position:relative">
            {{ $class->class_id }}
            <a style="position:absolute;right:0px;top:1px;padding-bottom:1px;padding-top:1px;"
               href="{{ route('grades.show', ['grade' => $class->id]) }}" class="btn btn-primary btn-sm active" role="button"
               aria-pressed="true">xem</a>
        </td>
        <td style="position:relative">
            <form action="{{ route('classes.destroy', ['grade' => $class->id]) }}" method="POST">
                <a style="position:absolute;right:60px;top:1px;padding-bottom:1px;padding-top:1px;" href="{{ route('classes.edit', ['grade' => $class->id]) }}" class="btn btn-primary btn-sm active" role="button" aria-pressed="true">Sửa</a>
                {{ method_field('DELETE') }}
                {{ csrf_field() }}
                <button style="position:absolute;top:1px;right:0px;padding-bottom:1px;padding-top:1px;" onclick="return del('{{ $class->id }}')" class="btn btn-danger btn-sm active">Xóa</button>
            </form>
        </td>
    </tr>
@endforeach
<script>
  /////////onclick delete ask/////
  function del(name){
      return confirm("bạn có chắc chắc muốn xóa bỏ không "+name+"?");
  }
</script>
