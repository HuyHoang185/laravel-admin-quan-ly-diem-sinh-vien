@extends('index')
@section('content')
    <div id="body">
        <div class="container">
            <a href="{{ route('classes.create') }}" class="btn btn-success btn-sm active" role="button"
               aria-pressed="true">Thêm danh mục</a>
            <div class="row content">
                <input type="text" name="classid" id="class_id" class="form-control"
                       style="width:100px;height:30px;margin-bottom:20px" placeholder="Tìm kiếm">
                <table>
                    <tr>
                        <th>ID</th>
                        <th>term_name</th>
                        <th>teacher_name</th>
                        <th>subject_name</th>
                        <th style="position:relative;">class_id</th>
                        <th>Action</th>
                    </tr>
                    <tbody id="demo">
                    </tbody>
                    <tbody id="test">
                    @foreach ($classes as $class)
                        <tr>
                            <td>{{ $class->id }}</td>
                            <td>{{ $class->term_name }}</td>
                            <td>{{ $class->teacher_name }}</td>
                            <td>{{ $class->subject_name }}</td>

                            <td style="position:relative">
                                {{ $class->class_id }}
                                <a style="position:absolute;right:0px;top:1px;padding-bottom:1px;padding-top:1px;"
                                   href="{{ route('grades.show', ['grade' => $class->id]) }}"
                                   class="btn btn-primary btn-sm active" role="button" aria-pressed="true">xem</a>
                            </td>
                            <td style="position:relative">
                                <form action="{{ route('classes.destroy', ['class' => $class->id]) }}" method="POST">
                                    <a style="position:absolute;right:60px;top:1px;padding-bottom:1px;padding-top:1px;"
                                       href="{{ route('classes.edit', ['class' => $class->id]) }}"
                                       class="btn btn-primary btn-sm active" role="button" aria-pressed="true">Sửa</a>
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button
                                        style="position:absolute;top:1px;right:0px;padding-bottom:1px;padding-top:1px;"
                                        onclick="return del('?')" class="btn btn-danger btn-sm active">Xóa
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <nav aria_label="Page navigation" style="margin:30px 0 0 75px">
            {{ $classes->links() }}
        </nav>
    </div>
@push('scripts')
    <script>
        /////////onclick delete ask/////
        function del(name) {
            return confirm("bạn có chắc chắc muốn xóa bỏ không " + name + "?");
        }

        //////// autocomplete search classid//////
        $(document).ready(function () {

            $('#class_id').keyup(function () { //bắt sự kiện keyup khi người dùng gõ từ khóa tim kiếm
                var query = $(this).val(); //lấy gía trị ng dùng gõ
                if (query != '') //kiểm tra khác rỗng thì thực hiện đoạn lệnh bên dưới
                {
                    var _token = $('input[name="_token"]').val(); // token để mã hóa dữ liệu
                    $.ajax({
                        url: "{{ route('search') }}",
                        // đường dẫn khi gửi dữ liệu đi 'search' là tên route mình đặt bạn mở route lên xem là hiểu nó là cái j.
                        method: "POST",
                        // phương thức gửi dữ liệu.
                        data: {query: query, _token: _token},
                        success: function (data) { //dữ liệu nhận về
                            $('#demo').fadeIn();
                            $('#demo').html(data); //nhận dữ liệu dạng html và gán vào cặp thẻ có id là countryList
                            $("#test").addClass('hidden')
                        }
                    });

                }
            });

        });
    </script>
@endpush
@endsection
