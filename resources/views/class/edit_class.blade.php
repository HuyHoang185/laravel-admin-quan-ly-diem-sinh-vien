@extends('index')
@section('content')
  <!-- body -->
  <div id="edit_product">
      <div class="container">
          <div class=" row place">
              <p>Home | Classes | Sửa class</p>
          </div>
          <div style="margin:40px 0 40px -15px;"><h3>Sửa class</h3></div>
          <div class="row">
              <div class="form-add">
                  <form method="post" enctype="multipart/form-data" action="{{ route('classes.update', $class->id) }}">
                      @method('PUT')
                      {{csrf_field()}}
                      <div class="form-group">
                          <label>Ki Hoc</label>
                          <input name="term_name" type="text" value="{{ $class->term_name }}">
                      </div>
                      <div class="form-group">
                          <label>Ten Mon Hoc</label>
                          <input name="subject_name" type="text" value="{{ $class->subject_name }}">
                      </div>
                      <div class="form-group">
                          <label>Ten Giao Vien</label>
                          <input name="teacher_name" type="text" value="{{ $class->teacher_name }}">
                      </div>
                      <div class="form-group">
                          <label>Lop</label>
                          <input name="grade" type="text" value="{{ $class->class_id }}">
                      </div>
                      <div class="form-group">
                          <input name="sbm" type="submit" role="" value="Cập nhật">
                      </div>
                  </form>
              </div>
          </div>
      </div>
  </div>
@endsection
