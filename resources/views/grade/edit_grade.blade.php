@extends('index')
@section('content')

  <!-- body -->
  <div id="edit_product">
        <div class="container">
            <div class=" row place">
                <p>Home | Grade | Sửa điểm</p>
            </div>
            <div style="margin:40px 0 40px -15px;"><h3>Sửa điểm</h3></div>
            <div class="row">
                <div class="form-add">
                    <form method="post" enctype="multipart/form-data" action="{{  route('grades.update', $grade->id) }}">
                    @method('PUT')
                    {{csrf_field()}}
                        <div class="form-group">
                            <label>Ten Hoc Sinh</label>
                            <input name="student_name" type="text" value="{{ $grade->student_id }}">
                        </div>
                        <div class="form-group">
                            <label>Ten Mon Hoc</label>
                            <input name="subject_name" type="text" value="{{ $grade->student_name }}">
                        </div>
                        <div class="form-group">
                            <label>Ki Hoc</label>
                            <input name="term" type="text" value="{{ $grade->term_name }}">
                        </div>
                        <div class="form-group">
                            <label>Diem</label>
                            <input name="grade" type="text" value="{{ $grade->mid_grade }}">
                        </div>
                        <div class="form-group">
                            <input name="sbm" type="submit" role="" value="Cập nhật">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
