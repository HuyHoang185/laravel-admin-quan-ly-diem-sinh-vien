@extends('index')
@section('content')
   <!-- body -->
   <div id="add_product">
       <div class="container">
           <div class=" row place">
               <p>Home | Product | Thêm sản phẩm</p>
           </div>
           <div style="margin:40px 0 40px -15px;"><h3>Thêm sản phẩm</h3></div>
           <div class="row">
               <div class="form-add">
                   <form method="post" enctype="multipart/form-data" action="{{ route('grades.store') }}">
                       {{csrf_field()}}
                       <div class="form-group">
                           <label>Tên học sinh</label>
                           <input name="student_name" type="text" value="">
                       </div>
                       <div class="form-group">
                           <label>Tên môn học</label>
                           <input name="subject_name" type="text" value="">
                       </div>
                       <div class="form-group">
                           <label>Tên giáo viên</label>
                           <input name="teacher_name" type="text" value="">
                       </div>
                       <div class="form-group">
                           <label>kì học</label>
                           <input name="term" type="text" value="">
                       </div>
                       <div class="form-group">
                           <label>Điểm</label>
                           <input name="grade" type="text" value="">
                       </div>
                       <div class="form-group">
                           <input name="sbm" type="submit" value="Thêm mới">
                       </div>
                   </form>
               </div>
           </div>
       </div>
   </div>
@endsection
