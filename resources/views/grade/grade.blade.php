@extends('index')
@section('content')

    <div id="body">
        <div class="container">
            <a href="{{ route('grades.create') }}" class="btn btn-success btn-sm active" role="button" aria-pressed="true">Thêm sản phẩm</a>
            <form action="{{ route('grades.show', ['grade' => $id]) }}">
                <div class="row">
                    <div class="col-lg-2">
                        <div class="form-group">
                            <label>MSSV</label>
                            <input type="text" name="student_id" id="student_id" value="{{ request('student_id')?request('student_id'):'' }}" class="form-control">
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label>Tên sinh viên</label>
                            <input type="text" name="student_name" id="student_name"
                                   value="{{ request('student_name')?request('student_name'):'' }}"
                                   class="form-control">
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <label></label>
                        <button type="submit" class="btn btn-info" style="margin-top:32px">Tìm kiếm</button>
                    </div>
                </div>
            </form>
            <div class="row content">
                <table>
                    <tr>
                        <th>id</th>
                        <th>id_student</th>
                        <th>student_name</th>
                        <th>class</th>
                        <th>term</th>
                        <th>mid_grade</th>
                        <th>final_grade</th>
                        <th>grade</th>
                        <th>Action</th>
                    </tr>
                    @foreach ( $grades as $grade )
                        <tr>
                            <td>{{ $grade->id }}</td>
                            <td>{{ $grade->student_id }}</td>
                            <td>{{ $grade->student_name }}</td>
                            <td>{{ $grade->class_id }}</td>
                            <td>{{ $grade->term_name }}</td>
                            <td>{{ $grade->mid_grade }}</td>
                            <td>{{ $grade->final_grade }}</td>
                            <td>{{ null }}</td>
                            <td style="position:relative">
                                <a style="position:absolute;right:60px;top:1px;padding-bottom:1px;padding-top:1px;"
                                   href="{{ route('grades.edit', ['grade' => $grade->id]) }}" class="btn btn-primary btn-sm active" role="button" aria-pressed="true">Sửa</a>
                                <form action="{{ route('grades.destroy', ['grade' => $grade->id]) }}" method="POST">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button
                                        style="position:absolute;top:1px;right:10px;padding-bottom:1px;padding-top:1px;"
                                        onclick="return del(' {{ $grade->grade_name }} ')"
                                        class="btn btn-danger btn-sm active">Xóa
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
            {{ $grades->appends(request()->query())->links() }}
        </div>
    </div>
    <script>
        function del(name) {
            return confirm("bạn có chắc chắc muốn xóa bỏ không " + name + "?");
        }
    </script>
@endsection
