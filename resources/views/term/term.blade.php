@extends('index')
@section('content')
 <!-- body -->
 <div id="body">
     <div class="container">
         <a href="{{ route('terms.create') }}" class="btn btn-success btn-sm active" role="button" aria-pressed="true">Thêm
             danh mục</a>
         <div class="row content">
             <table>
                 <tr>
                     <th>id</th>
                     <th>term_name</th>
                     <th>Action</th>
                 </tr>
                 @foreach($terms as $term)
                     <tr>
                         <td>{{ $term->id }}</td>
                         <td>{{ $term->term_name }}</td>
                         <td style="position:relative">
                             <a href="{{ route('terms.edit', ['term' => $term->id]) }}"
                                class="btn btn-primary btn-sm active"
                                role="button" aria-pressed="true">Sửa</a>
                             <form action="{{ route('terms.destroy', ['term' => $term->id]) }}" method="POST">
                                 {{ method_field('DELETE') }}
                                 {{ csrf_field() }}
                                 <button onclick="return del('{{ $term->term_name }}')"
                                         style="position:absolute;top:1px;right:110px"
                                         class="btn btn-danger btn-sm active">Xóa
                                 </button>
                             </form>
                         </td>
                     </tr>
                 @endforeach
             </table>
         </div>
     </div>
     <nav aria_label="Page navigation" style="margin:30px 0 0 75px">
         {{ $terms->links() }}
     </nav>
 </div>
 <script>
     function del(name) {
         return confirm("bạn có chắc chắc muốn xóa bỏ không " + name + "?");
     }
 </script>
@endsection
