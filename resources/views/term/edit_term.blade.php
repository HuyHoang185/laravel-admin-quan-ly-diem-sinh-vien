@extends('index')
@section('content')
    <!-- body -->
    <div id="edit_cat">
        <div class="container">
            <div class=" row place">
                <p>Home | Teachers | Sửa tên giáo viên</p>
            </div>
            <div style="margin:40px 0 40px -15px;"><h3>Sửa tên giáo viên</h3></div>
            <div class="row">
                <form class="form" method="post" action="{{ route('terms.update', ['term' => $term->id) }}">
                @method('PUT')
                {{ csrf_field() }}
                    <label>Tên giáo viên</label>
                    <input name="term_name" type="" value="{{ $term->term_name }}" placeholder="Tên giáo viên">
                    <input name="sbm" type="submit" value="Cập nhật">
                </form>
            </div>
        </div>
    </div>
@endsection
