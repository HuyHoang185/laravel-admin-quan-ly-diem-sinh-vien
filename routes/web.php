<?php

use App\Http\Controllers\ClassesController;
use App\Http\Controllers\GradeController;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\SubjectController;
use App\Http\Controllers\TeacherController;
use App\Http\Controllers\TermController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('index');
});
Route::get('admin', [GradeController::class, 'index']);
Route::group(['prefix' => 'admin'], function() {
    Route::resource('grades', GradeController::class);
    Route::resource('students', StudentController::class);
    Route::resource('teachers', TeacherController::class);
    Route::resource('subjects', SubjectController::class);
    Route::resource('terms', TermController::class);
    Route::resource('classes', ClassesController::class);
});
Route::group(['prefix' => 'data'], function() {
    Route::get('insert_class', [ClassesController::class, 'insert_class']);
    Route::get('insert_student', [StudentController::class, 'insert_student']);
    Route::get('insert_grade', [GradeController::class, 'insert_grade']);
});
Route::post('search/name', [ClassesController::class, 'getSearchAjax'])->name('search');
